from django import forms
from .models import Names
from django.forms import ModelForm

class NameForm(forms.ModelForm):
    class Meta:
        model=Names
        fields='__all__'