from django.shortcuts import render,redirect
from .models import Names
from .forms import NameForm
def home(request):
    return render(request,'tester/home.html')
def index(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            data_item= form.save(commit=False)
            data_item.save()
        return redirect('/')
    form= NameForm()
    data= Names.objects.all()
    response={'Data':data,'status':form}
    return render(request,'tester/home.html',response)

