from django.test import TestCase,Client
from django.urls import resolve
from .views import home,index
from .models import Names
class story6test(TestCase):
    def test1(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)
    def test2(self):
        found= resolve('/')
        self.assertEqual(found.func,index)
    def testmodeltest(self):
        Names.objects.create(name='Wassup')
        countcontent=Names.objects.all().count()
        self.assertEqual(countcontent,1)
    def testhtmltest(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,'tester/home.html')
    